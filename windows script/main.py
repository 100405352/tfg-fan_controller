
import clr  # pythonnet
import serial, time
import warnings
import serial.tools.list_ports

cpuTemp = 0
gpuTemp = 0
openhardwaremonitor_sensortypes = ['Voltage', 'Clock', 'Temperature', 'Load', 'Fan', 'Flow', 'Control', 'Level', 'Factor', 'Power', 'Data', 'SmallData']


def initialize():
    file = "LibreHardwareMonitorLib"
    clr.AddReference(file)

    from LibreHardwareMonitor import Hardware

    handle = Hardware.Computer()
    handle.IsCpuEnabled = True
    handle.IsGpuEnabled = True
    handle.Open()
    return handle


def fetch_stats(handle):
    for i in handle.Hardware:
        i.Update()
        for sensor in i.Sensors:
            parse_sensor(sensor)


def parse_sensor(sensor):
    if sensor.Value is not None:
        if type(sensor).__module__ == 'LibreHardwareMonitor.Hardware':
            sensortypes = openhardwaremonitor_sensortypes
        else:
            return
        if str(sensor.SensorType) == sensortypes[2]:
            if str(sensor.Name) == 'CPU Package':
                global cpuTemp
                cpuTemp = sensor.Value
                print("Temperatura CPU:", cpuTemp)
            elif str(sensor.Name) == 'GPU Core':
                global gpuTemp
                gpuTemp = sensor.Value
                print("Temperatura GPU:", gpuTemp)


def com_connection():
    while True:
        arduino_ports_list = [
            p.device
            for p in serial.tools.list_ports.comports()
            if 'Arduino' in p.description
        ]
        if not arduino_ports_list:
            warnings.simplefilter('always', UserWarning)
            warnings.warn("No Arduino found")
        elif len(arduino_ports_list) > 1:
            warnings.warn('Multiple Arduinos found ...using the first')
        else:
            return serial.Serial(port=arduino_ports_list[0], baudrate=9600, timeout=1, write_timeout=1)
        time.sleep(2)


def main():
    com_details = com_connection()
    HardwareHandle = initialize()
    with com_details:
        while True:
            fetch_stats(HardwareHandle)
            byte1 = 'CPU: ' + str(cpuTemp) + '\n'
            byte2 = 'GPU: ' + str(gpuTemp) + '\n'
            try:
                com_details.write(byte1.encode('utf-8'))
                com_details.write(byte2.encode('utf-8'))
                time.sleep(2)
            except serial.SerialException:
                com_details.close()
                main()
            except KeyboardInterrupt:
                com_details.close()
                warnings.warn("Stopped by user. Ending...")
                exit()


if __name__ == "__main__":
    main()
