package com.example.newapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Switch;
import android.os.Handler;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;
import java.util.UUID;

public class MainActivity extends Activity {
    private final String DEVICE_ADDRESS = "00:22:11:30:ED:F6";
    String transitString, string1, string2, cpuTemp, gpuTemp;
    private final UUID PORT_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    Button startButton, stopButton, setSpeed;
    TextView showGpuTemp, showCpuTemp, textFan1, textFan2, textFan3, textFan4;
    EditText m1_data, m2_data, m3_data, m4_data;
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    Switch defaultSwitch, silentSwitch, HighPerformanceSwitch, manualSwitch;
    boolean deviceConnected = false;
    byte buffer[];
    boolean stopThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton = findViewById(R.id.buttonStart);
        stopButton = findViewById(R.id.buttonStop);
        showGpuTemp = findViewById(R.id.showGpuTemp);
        showCpuTemp = findViewById(R.id.showCpuTemp);
        defaultSwitch = findViewById(R.id.switch_def);
        silentSwitch = findViewById(R.id.switch_sil);
        HighPerformanceSwitch = findViewById(R.id.switch_hih);
        manualSwitch = findViewById(R.id.switch_man);
        textFan1 = findViewById(R.id.textFan1);
        textFan2 = findViewById(R.id.textFan2);
        textFan3 = findViewById(R.id.textFan3);
        textFan4 = findViewById(R.id.textFan4);
        m1_data = findViewById(R.id.m1_data);
        m2_data = findViewById(R.id.m2_data);
        m3_data = findViewById(R.id.m3_data);
        m4_data = findViewById(R.id.m4_data);
        setSpeed = findViewById(R.id.buttonSetSpeed);
        setUiEnabled(false);
        setUiEnabledForManualProfile(false);


        defaultSwitch.setOnClickListener(view -> {
            if (defaultSwitch.isChecked()) {
                silentSwitch.setChecked(false);
                HighPerformanceSwitch.setChecked(false);
                manualSwitch.setChecked(false);
            } else {
                defaultSwitch.setChecked(true);
            }
            setUiEnabledForManualProfile(false);
            changeMode("DEF\n");
        });

        silentSwitch.setOnClickListener(view -> {
            if (silentSwitch.isChecked()) {
                defaultSwitch.setChecked(false);
                HighPerformanceSwitch.setChecked(false);
                manualSwitch.setChecked(false);
                setUiEnabledForManualProfile(false);
                changeMode("SIL\n");
            } else {
                defaultSwitch.setChecked(true);
                setUiEnabledForManualProfile(false);
                changeMode("DEF\n");
            }
        });

        HighPerformanceSwitch.setOnClickListener(view -> {
            if (HighPerformanceSwitch.isChecked()) {
                defaultSwitch.setChecked(false);
                silentSwitch.setChecked(false);
                manualSwitch.setChecked(false);
                setUiEnabledForManualProfile(false);
                changeMode("HIH\n");
            } else {
                defaultSwitch.setChecked(true);
                setUiEnabledForManualProfile(false);
                changeMode("DEF\n");
            }
        });

        manualSwitch.setOnClickListener(view -> {
            if (manualSwitch.isChecked()) {
                defaultSwitch.setChecked(false);
                silentSwitch.setChecked(false);
                HighPerformanceSwitch.setChecked(false);
                setUiEnabledForManualProfile(true);
                changeMode("MAN\n");
            } else {
                defaultSwitch.setChecked(true);
                setUiEnabledForManualProfile(false);
                changeMode("DEF\n");
            }
        });

    }

    public void changeMode(String mode) {
        try {
            outputStream.write(mode.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String checkDigits(String str) {
        if (str.equals("")) {
            return "0";
        } else if (Integer.parseInt(str) > 100) {
            return "100";
        } else return str;
    }

    public void sendSepeedSettings(View view) {
        String fan1_value = m1_data.getText().toString();
        String str1 = "FAN1: ".concat(checkDigits(fan1_value) + "\n");
        String fan2_value = m2_data.getText().toString();
        String str2 = "FAN2: ".concat(checkDigits(fan2_value) + "\n");
        String fan3_value = m3_data.getText().toString();
        String str3 = "FAN3: ".concat(checkDigits(fan3_value) + "\n");
        String fan4_value = m4_data.getText().toString();
        String str4 = "FAN4: ".concat(checkDigits(fan4_value) + "\n");
        try {
            outputStream.write(str1.getBytes());
            outputStream.write(str2.getBytes());
            outputStream.write(str3.getBytes());
            outputStream.write(str4.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        //textView.append("\nSent Data:" + string + "\n");
    }

    public void setUiEnabled(boolean bool) {
        startButton.setEnabled(!bool);
        stopButton.setEnabled(bool);
        showGpuTemp.setEnabled(bool);
        showCpuTemp.setEnabled(bool);
        defaultSwitch.setEnabled(bool);
        silentSwitch.setEnabled(bool);
        HighPerformanceSwitch.setEnabled(bool);
        manualSwitch.setEnabled(bool);
    }

    public void setUiEnabledForManualProfile(boolean bool) {
        textFan1.setEnabled(bool);
        textFan2.setEnabled(bool);
        textFan3.setEnabled(bool);
        textFan4.setEnabled(bool);
        m1_data.setEnabled(bool);
        m2_data.setEnabled(bool);
        m3_data.setEnabled(bool);
        m4_data.setEnabled(bool);
        setSpeed.setEnabled(bool);
    }

    public boolean BTinit() {
        boolean found = false;
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(getApplicationContext(), "Device doesnt Support Bluetooth", Toast.LENGTH_SHORT).show();
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableAdapter = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableAdapter, 0);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        System.out.println("Bond devices: " + bondedDevices);
        if (bondedDevices.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Pair the Device first", Toast.LENGTH_SHORT).show();
        } else {
            for (BluetoothDevice iterator : bondedDevices) {
                if (iterator.getAddress().equals(DEVICE_ADDRESS)) {
                    device = iterator;
                    found = true;
                    break;
                }
            }
        }

        return found;
    }

    public boolean BTconnect() {
        boolean connected = true;
        try {
            socket = device.createRfcommSocketToServiceRecord(PORT_UUID);
            socket.connect();
        } catch (IOException e) {
            e.printStackTrace();
            connected = false;
        }
        if (connected) {
            try {
                outputStream = socket.getOutputStream();
            } catch (IOException a) {
                a.printStackTrace();
            }
            try {
                inputStream = socket.getInputStream();
            } catch (IOException b) {
                b.printStackTrace();
            }
        }
        return connected;
    }

    public void onClickStart(View view) {
        if (BTinit()) {
            if (BTconnect()) {
                setUiEnabled(true);
                deviceConnected = true;
                beginListenForData();
            }
        }
    }

    void beginListenForData() {
        final Handler handler = new Handler();
        stopThread = false;
        buffer = new byte[1024];
        Thread thread = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted() && !stopThread) {
                try {
                    int byteCount = inputStream.available();
                    if (byteCount > 0) {
                        byte[] rawBytes = new byte[byteCount];
                        inputStream.read(rawBytes);
                        final String convertedUTFString = new String(rawBytes, StandardCharsets.UTF_8);
                        transitString += convertedUTFString;
                        if (transitString.charAt(transitString.length() - 1) == '\n') {
                            // Separamos los dos string que llegan
                            if (transitString.length() >= 12) {
                                boolean firstWord = true;
                                for (int i = 0; i < transitString.length() - 1; i++) {
                                    char tmpChar = transitString.charAt(i);
                                    if (firstWord && tmpChar != '\n') {
                                        string1 += tmpChar;
                                    } else if (tmpChar != '\n' && !firstWord) {
                                        string2 += tmpChar;
                                    } else {
                                        firstWord = false;
                                    }
                                }
                            } else {
                                string1 = transitString;
                            }
                            transitString = "";
                            dataFilter(string1);
                            dataFilter(string2);
                            string1 = "";
                            string2 = "";
                            handler.post(() -> {
                                showCpuTemp.setText(cpuTemp.concat("°C"));
                                showGpuTemp.setText(gpuTemp.concat("°C"));
                            });
                        }


                    }
                } catch (IOException ex) {
                    stopThread = true;
                }
            }
        });

        thread.start();
    }

    private void dataFilter(String str) {
        if (str != null && str.contains("CPU")) {
            cpuTemp = extractDigits(str);
        } else if (str != null && str.contains("GPU")) {
            gpuTemp = extractDigits(str);
        }
    }

    private String extractDigits(String str) {
        return str.replaceAll("[^0-9\\.]", "");
    }

    public void onClickStop(View view) throws IOException {
        stopThread = true;
        outputStream.close();
        inputStream.close();
        socket.close();
        setUiEnabled(false);
        deviceConnected = false;
    }
}
