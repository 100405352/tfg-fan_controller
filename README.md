# TFG-Fan_Controller



## windows script

It contains a script for Windows 10/11 that allows retrieving the system temperature and sending it to Arduino.

Contiene un script para Windows 10/11 que permite recuperar la temperatura del sistema y enviarla al Arduino.

## linux script

It contains a script for GNU/Linux that allows retrieving the system temperature and sending it to Arduino.

Contiene un script para GNU/Linux que permite recuperar la temperatura del sistema y enviarla al Arduino.

## arduino code

It contains all the Arduino code that allows controlling multiple fans using different fuzzy logic models.

Contiene todo el código de Arduino que permite controlar hasta ventiladores mediante diferentes modelos de lógica difusa.

## mobile app

An Android app that receives temperature values and allows selecting different profiles that change the behavior of the control system.

Aplicación Android que recibe los valores de temperatura y permite seleccionar diferentes perfiles que cambian el comportamiento del sistema de control.
