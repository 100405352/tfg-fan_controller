import psutil
import GPUtil
import re
import serial
import serial.tools.list_ports
import time
import warnings

cpuTemp = 0
gpuTemp = 0


def fetch_stats():
    temps = psutil.sensors_temperatures()
    for name, entries in temps.items():
        if name != 'coretemp':
            continue
        for entry in entries:
            if re.search('PACKAGE', entry.label, re.IGNORECASE):
                global cpuTemp
                cpuTemp = entry.current
                print("Temperatura CPU:", cpuTemp)
    gpu_data = GPUtil.getGPUs()
    for gpu in gpu_data:
        global gpuTemp
        gpuTemp = gpu.temperature
        print("Temperatura GPU:", gpuTemp)


def com_connection():
    while True:
        arduino_ports_list = [
            p.device
            for p in serial.tools.list_ports.comports()
            if 'Arduino' in p.manufacturer
        ]
        if not arduino_ports_list:
            warnings.simplefilter('always', UserWarning)
            warnings.warn("No Arduino found")
        elif len(arduino_ports_list) > 1:
            warnings.warn('Multiple Arduinos found ...using the first')
        else:
            return serial.Serial(port=arduino_ports_list[0], baudrate=9600, timeout=1, write_timeout=1)
        time.sleep(2)


def main():
    com_details = com_connection()
    with com_details:
        while True:
            fetch_stats()
            byte1 = 'CPU: ' + str(cpuTemp) + '\n'
            byte2 = 'GPU: ' + str(gpuTemp) + '\n'
            try:
                com_details.write(byte1.encode('utf-8'))
                com_details.write(byte2.encode('utf-8'))
                time.sleep(2)
            except serial.SerialException:
                com_details.close()
                main()
            except KeyboardInterrupt:
                com_details.close()
                warnings.warn("Stopped by user. Ending...")
                exit()


if __name__ == "__main__":
    main()

