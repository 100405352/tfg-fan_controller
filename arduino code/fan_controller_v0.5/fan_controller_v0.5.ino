// -------------------------------------- //
// Include files                          //
// -------------------------------------- //
#include <Adafruit_MotorShield.h>
#include <SoftwareSerial.h>
#include <DHT.h>
#include "common_func.h"
#include "default_profile.h"
#include "silence_profile.h"
#include "high_profile.h"

// -------------------------------------- //
// Global variables                       //
// -------------------------------------- //
bool message_received = false;
bool message_processed = false;
float cpuTemp;
float gpuTemp;
float caseTemp;
float cpuSetPoint;
float gpuSetPoint;
String inputString;
String request[2];
byte mode = 2;
byte speed_fan1;
byte speed_fan2;
byte speed_fan3;
byte speed_fan4;

// -------------------------------------- //
// Global constants                       //
// -------------------------------------- //
const byte threshold = 5;

// -------------------------------------- //
// Object creation                        //
// -------------------------------------- //
Adafruit_MotorShield AFMS = Adafruit_MotorShield();

Adafruit_DCMotor *motor1 = AFMS.getMotor(1);  // Top 2
Adafruit_DCMotor *motor2 = AFMS.getMotor(2);  // Back
Adafruit_DCMotor *motor3 = AFMS.getMotor(3);  // Top 1
Adafruit_DCMotor *motor4 = AFMS.getMotor(4);  // Bot

// -------------------------------------- //
// Sensor initialization                  //
// -------------------------------------- //
SoftwareSerial BTSerial(4, 5);
DHT dht(3, DHT11);

// -------------------------------------- //
// Function: serial_event                 //
// -------------------------------------- //
void serial_event() {
  static byte count = 0;
  if (message_received) {
    if (!message_processed) {
      Serial.println("Warning: Unprocessed message");
    }
    message_received = false;
    message_processed = false;
  }
  while (Serial.available()) {
    char inputChar = Serial.read();
    if (((inputChar < 'A') || (inputChar > 'Z')) && ((inputChar < '0') || (inputChar > '9')) && (inputChar != ':') && (inputChar != '.') && (inputChar != ' ') && (inputChar != '\n')) {
      continue;
    }
    inputString += inputChar;
    if ((inputString[count] == '\n')) {
      count = 0;
      message_received = true;
      // Convert inputString into char[] format
      char chunk[inputString.length()];
      inputString.toCharArray(chunk, inputString.length());
      inputString = "";

      // Extract serial input into two array positions
      char *str;
      str = strtok(chunk, " -,;:");
      for (int i = 0; i < 2; i++) {
        printf("%s\n", str);
        request[i] = str;
        str = strtok(NULL, " -,;:");
      }

      bool isNumeric = true;
      for (int i = 0; i < request[1].length(); i++) {
        if (isDigit(request[1][0]) == 0 && request[1][0] != '.') {
          isNumeric = false;
        }
      }

      // Assigns the value to a variable
      if (isNumeric) {
        if (request[0].equals("CPU")) {
          cpuTemp = request[1].toFloat();
        } else if (request[0].equals("GPU")) {
          gpuTemp = request[1].toFloat();
        }
      }
      str = {};
      break;
    }
    count++;
  }
}

// -------------------------------------- //
// Function: BTserial_event               //
// -------------------------------------- //
void BTserial_event() {
  static int count = 0;
  //if (message_received) {
  //  message_received = false;
  //}

  while (BTSerial.available()) {
    char inputChar = BTSerial.read();
    if (((inputChar < 'A') || (inputChar > 'Z')) && ((inputChar < '0') || (inputChar > '9')) && (inputChar != ':') && (inputChar != '.') && (inputChar != ' ') && (inputChar != '\n')) {
      continue;
    }
    inputString += inputChar;
    if ((inputString[count] == '\n')) {
      count = 0;
      //message_received = true;

      // Convert inputString into char[] format
      char chunk[inputString.length()];
      inputString.toCharArray(chunk, inputString.length());
      inputString = "";

      // Extract serial input into two array positions
      char *str;
      str = strtok(chunk, " -,;:");
      for (int i = 0; i < 2; i++) {
        printf("%s\n", str);
        request[i] = str;
        str = strtok(NULL, " -,;:");
      }

      bool isNumeric = true;
      for (int i = 0; i < request[1].length(); i++) {
        if (isDigit(request[1][0]) == 0 && request[1][0] != '.') {
          isNumeric = false;
        }
      }

      // Assigns the value to a variable
      if (isNumeric) {
        if (request[0].equals("FAN1")) {
          speed_fan1 = map(request[1].toInt(), 0, 100, 0, 255);
        } else if (request[0].equals("FAN2")) {
          speed_fan2 = map(request[1].toInt(), 0, 100, 0, 255);
        } else if (request[0].equals("FAN3")) {
          speed_fan3 = map(request[1].toInt(), 0, 100, 0, 255);
        } else if (request[0].equals("FAN4")) {
          speed_fan4 = map(request[1].toInt(), 0, 100, 0, 255);
        }
      }

      // Sets the profile
      if (request[0].equals("DEF")) {
        mode = 1;
      } else if (request[0].equals("SIL")) {
        mode = 2;
      } else if (request[0].equals("HIH")) {
        mode = 3;
      } else if (request[0].equals("MAN")) {
        mode = 4;
      }
      str = {};
      break;
    }
    count++;
  }
}

// -------------------------------------- //
// Function: check_temperature            //
// -------------------------------------- //

boolean check_temperature(float *setPoint, float currentTemp) {
  if (currentTemp >= *setPoint + threshold) {
    *setPoint = currentTemp;
    return true;
  } else if (currentTemp <= *setPoint - threshold) {
    *setPoint = currentTemp;
    return true;
  } else {
    return false;
  }
}

// -------------------------------------- //
// Function: cpu_temp_reading             //
// -------------------------------------- //
void cpu_temp_reading() {
  if ((message_received) && (!message_processed) && (request[0].equals("CPU"))) {
    message_processed = true;
    inputString = "";
  }
}

// -------------------------------------- //
// Function: gpu_temp_reading             //
// -------------------------------------- //
void gpu_temp_reading() {
  if ((message_received) && (!message_processed) && (request[0].equals("GPU"))) {
    message_processed = true;
    inputString = "";
  }
}

// -------------------------------------- //
// Function: case_temp_reading            //
// -------------------------------------- //
void case_temp_reading() {
  caseTemp = dht.readTemperature();
}

// -------------------------------------- //
// Function: cpu_fan_control              //
// -------------------------------------- //
void cpu_fan_control(float pwm_value) {
  if (pwm_value < 51) {
    motor2->run(RELEASE);
    motor3->run(RELEASE);
  } else {
    motor2->run(FORWARD);
    motor2->setSpeed(pwm_value);
    motor3->run(FORWARD);
    motor3->setSpeed(pwm_value);
  }
}

// -------------------------------------- //
// Function: gpu_fan_control              //
// -------------------------------------- //
void gpu_fan_control(float pwm_value) {
  if (pwm_value < 51) {
    motor1->run(RELEASE);
    motor4->run(RELEASE);
  } else {
    motor1->run(FORWARD);
    motor1->setSpeed(pwm_value);
    motor4->run(FORWARD);
    motor4->setSpeed(pwm_value);
  }
}

// -------------------------------------- //
// Function: manual_fan_control           //
// -------------------------------------- //
void manual_fan_control() {
  set_fan_speed(motor1, speed_fan1);
  set_fan_speed(motor2, speed_fan2);
  set_fan_speed(motor3, speed_fan3);
  set_fan_speed(motor4, speed_fan4);
}

// -------------------------------------- //
// Function: set_fan_speed                //
// -------------------------------------- //
void set_fan_speed(Adafruit_DCMotor *fan, byte pwm) {
  if (pwm < 51) {
    fan->run(RELEASE);
  } else {
    fan->run(FORWARD);
    fan->setSpeed(pwm);
  }
}

// -------------------------------------- //
// Function: mode_1                       //
// -------------------------------------- //
void mode_1() {
  BTserial_event();
  serial_event();
  cpu_temp_reading();
  gpu_temp_reading();
  case_temp_reading();
  cpu_temp_sending();
  gpu_temp_sending();
  //////////////////////////////////////////
  if (check_temperature(&cpuSetPoint, cpuTemp)) {
    g_inputs_default[0] = cpuTemp;
    g_inputs_default[1] = caseTemp;
    g_outputs_default[0] = 0;
    init_default();
    cpu_fan_control(g_outputs_default[0]);
  }
  if (check_temperature(&gpuSetPoint, gpuTemp)) {
    g_inputs_default[0] = gpuTemp;
    g_inputs_default[1] = caseTemp;
    g_outputs_default[0] = 0;
    init_default();
    gpu_fan_control(g_outputs_default[0]);
  }
  //////////////////////////////////////////
}
// -------------------------------------- //
// Function: mode_2                       //
// -------------------------------------- //
void mode_2() {
  BTserial_event();
  serial_event();
  cpu_temp_reading();
  gpu_temp_reading();
  case_temp_reading();
  cpu_temp_sending();
  gpu_temp_sending();
  //////////////////////////////////////////
  if (check_temperature(&cpuSetPoint, cpuTemp)) {
    g_inputs_silence[0] = cpuTemp;
    g_outputs_silence[0] = 0;
    init_silence();
    cpu_fan_control(g_outputs_silence[0]);
  }
  if (check_temperature(&gpuSetPoint, gpuTemp)) {
    g_inputs_silence[0] = gpuTemp;
    g_outputs_silence[0] = 0;
    init_silence();
    gpu_fan_control(g_outputs_silence[0]);
  }
  //////////////////////////////////////////
}

// -------------------------------------- //
// Function: mode_3                       //
// -------------------------------------- //
void mode_3() {
  BTserial_event();
  serial_event();
  cpu_temp_reading();
  gpu_temp_reading();
  case_temp_reading();
  cpu_temp_sending();
  gpu_temp_sending();
  //////////////////////////////////////////
  if (check_temperature(&cpuSetPoint, cpuTemp)) {
    g_inputs_high[0] = cpuTemp;
    g_outputs_high[0] = 0;
    init_high();
    cpu_fan_control(g_outputs_high[0]);
  }
  if (check_temperature(&gpuSetPoint, gpuTemp)) {
    g_inputs_high[0] = gpuTemp;
    g_outputs_high[0] = 0;
    init_high();
    gpu_fan_control(g_outputs_high[0]);
  }
  //////////////////////////////////////////
}

// -------------------------------------- //
// Function: mode_4                       //
// -------------------------------------- //
void mode_4() {
  BTserial_event();
  serial_event();
  cpu_temp_reading();
  gpu_temp_reading();
  case_temp_reading();
  cpu_temp_sending();
  gpu_temp_sending();
  //////////////////////////////////////////
  manual_fan_control();
  //////////////////////////////////////////
}

// -------------------------------------- //
// Function: cpu_temp_sending             //
// -------------------------------------- //
void cpu_temp_sending() {
  char buffer[20] = " ";
  dtostrf(cpuTemp, 3, 2, buffer);
  BTSerial.write("CPU: ");
  BTSerial.write(buffer);
  BTSerial.write('\n');
}

// -------------------------------------- //
// Function: gpu_temp_sending             //
// -------------------------------------- //
void gpu_temp_sending() {
  char buffer[20] = " ";
  dtostrf(gpuTemp, 3, 2, buffer);
  BTSerial.write("GPU: ");
  BTSerial.write(buffer);
  BTSerial.write('\n');
}

// -------------------------------------- //
// Function: init_fans                    //
// -------------------------------------- //
void init_fans() {
  motor1->run(FORWARD);
  motor1->setSpeed(255);
  motor2->run(FORWARD);
  motor2->setSpeed(255);
  motor3->run(FORWARD);
  motor3->setSpeed(255);
  motor4->run(FORWARD);
  motor4->setSpeed(255);
}

// -------------------------------------- //
// Function: setup                        //
// -------------------------------------- //
void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0));
  BTSerial.begin(9600);

  if (!AFMS.begin(25)) {
    Serial.println("Could not find Motor Shield.");
    while (1)
      ;
  }
  init_fans();
}

// -------------------------------------- //
// Function: loop                         //
// -------------------------------------- //
void loop() {
  delay(2000);
  switch (mode) {
    case 1:
      mode_1();
      break;
    case 2:
      mode_2();
      break;
    case 3:
      mode_3();
      break;
    case 4:
      mode_4();
      break;
  }
}
