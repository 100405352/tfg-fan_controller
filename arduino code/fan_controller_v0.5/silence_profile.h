// -------------------------------------- //
// Data for Fuzzy Inference System        //
// -------------------------------------- //
// Number of inputs to the FIS
const byte n_inputs_silence = 1;
// Number of outputs to the FIS
const byte n_outputs_silence = 1;


float g_inputs_silence[n_inputs_silence];
float g_outputs_silence[n_outputs_silence];

void init_silence() {
  // Number of rules to the FIS
  const byte n_rules_silence = 6;
  // Count of member function for each Input
  byte n_mf_input_silence[] = {6};

  // Count of member function for each Output
  byte n_mf_output_silence[] = {5};

  // Coefficients for the Input Member Functions
  float coef_input_mf1_silence[] = {0, 0, 30};
  float coef_input_mf2_silence[] = {10, 30, 50};
  float coef_input_mf3_silence[] = {30, 50, 70};
  float coef_input_mf4_silence[] = {50, 65, 80};
  float coef_input_mf5_silence[] = {70, 85, 100};
  float coef_input_mf6_silence[] = {80, 100, 100};
  float *g_coef_input_mf_silence[] = {coef_input_mf1_silence, coef_input_mf2_silence, coef_input_mf3_silence,
                                      coef_input_mf4_silence, coef_input_mf5_silence, coef_input_mf6_silence
                                     };
  float **g_fis_coef_input_mf_silence[] = {g_coef_input_mf_silence};

  // Coefficients for the Output Member Functions
  float coef_output_mf1_silence[] = {0, 0};
  float coef_output_mf2_silence[] = {0, 64};
  float coef_output_mf3_silence[] = {0, 96};
  float coef_output_mf4_silence[] = {0, 127};
  float coef_output_mf5_silence[] = {0, 255};
  float *g_coef_output_mf_silence[] = {coef_output_mf1_silence, coef_output_mf2_silence, coef_output_mf3_silence,
                                       coef_output_mf4_silence, coef_output_mf5_silence
                                      };
  float **g_fis_coef_output_mf_silence[] = {g_coef_output_mf_silence};

  // Input membership function set
  byte input_mf_set_silence[] = {0, 0, 0, 0, 0, 0};
  byte *g_input_mf_set_silence[] = {input_mf_set_silence};

  // Rule Weights
  float rule_weight_silence[] = {1, 1, 1, 1, 1, 1};

  // Rule Type
  byte rule_type_silence[] = {1, 1, 1, 1, 1, 1};

  // Rule Inputs
  byte rule_input1_silence[] = {1};
  byte rule_input2_silence[] = {2};
  byte rule_input3_silence[] = {3};
  byte rule_input4_silence[] = {4};
  byte rule_input5_silence[] = {5};
  byte rule_input6_silence[] = {6};
  byte *g_rule_input_silence[] = {rule_input1_silence, rule_input2_silence, rule_input3_silence, rule_input4_silence,
                                  rule_input5_silence, rule_input6_silence
                                 };

  // Rule Outputs
  byte rule_output1_silence[] = {1};
  byte rule_output2_silence[] = {1};
  byte rule_output3_silence[] = {2};
  byte rule_output4_silence[] = {3};
  byte rule_output5_silence[] = {4};
  byte rule_output6_silence[] = {5};
  byte *g_rule_output_silence[] = {rule_output1_silence, rule_output2_silence, rule_output3_silence, rule_output4_silence,
                                   rule_output5_silence, rule_output6_silence
                                  };

  // Output range Min
  float output_range_min_silence[] = {0};

  // Output range Max
  float output_range_max_silence[] = {1};

  float fuzzy_input_silence[] = {0, 0, 0, 0, 0, 0};
  float *g_fuzzy_input_silence[n_inputs_silence] = {fuzzy_input_silence,};

  fis_evaluate(n_inputs_silence, n_outputs_silence, n_rules_silence, g_inputs_silence, g_outputs_silence,
               n_mf_input_silence, n_mf_output_silence, g_fis_coef_input_mf_silence, g_fis_coef_output_mf_silence,
               g_input_mf_set_silence, rule_weight_silence, rule_type_silence, g_rule_input_silence,
               g_rule_output_silence, output_range_min_silence, output_range_max_silence, g_fuzzy_input_silence);

}
