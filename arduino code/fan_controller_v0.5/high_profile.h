// -------------------------------------- //
// Data for Fuzzy Inference System        //
// -------------------------------------- //
// Number of inputs to the FIS
const byte n_inputs_high = 1;
// Number of outputs to the FIS
const byte n_outputs_high = 1;


float g_inputs_high[n_inputs_high];
float g_outputs_high[n_outputs_high];

void init_high() {
  // Number of rules to the FIS
  const byte n_rules_high = 6;
  // Count of member function for each Input
  byte n_mf_input_high[] = {6};

  // Count of member function for each Output
  byte n_mf_output_high[] = {5};

  // Coefficients for the Input Member Functions
  float coef_input_mf1_high[] = {0, 0, 20};
  float coef_input_mf2_high[] = {0, 20, 40};
  float coef_input_mf3_high[] = {20, 40, 60};
  float coef_input_mf4_high[] = {40, 57.5, 75};
  float coef_input_mf5_high[] = {60, 75, 90};
  float coef_input_mf6_high[] = {75, 100, 100};
  float *g_coef_input_mf_high[] = {coef_input_mf1_high, coef_input_mf2_high, coef_input_mf3_high,
                                   coef_input_mf4_high, coef_input_mf5_high, coef_input_mf6_high
                                  };
  float **g_fis_coef_input_mf_high[] = {g_coef_input_mf_high};

  // Coefficients for the Output Member Functions
  float coef_output_mf1_high[] = {0, 64};
  float coef_output_mf2_high[] = {0, 127};
  float coef_output_mf3_high[] = {0, 159};
  float coef_output_mf4_high[] = {0, 191};
  float coef_output_mf5_high[] = {0, 255};
  float *g_coef_output_mf_high[] = {coef_output_mf1_high, coef_output_mf2_high, coef_output_mf3_high,
                                    coef_output_mf4_high, coef_output_mf5_high
                                   };
  float **g_fis_coef_output_mf_high[] = {g_coef_output_mf_high};

  // Input membership function set
  byte input_mf_set_high[] = {0, 0, 0, 0, 0, 0};
  byte *g_input_mf_set_high[] = {input_mf_set_high};

  // Rule Weights
  float rule_weight_high[] = {1, 1, 1, 1, 1, 1};

  // Rule Type
  byte rule_type_high[] = {1, 1, 1, 1, 1, 1};

  // Rule Inputs
  byte rule_input1_high[] = {1};
  byte rule_input2_high[] = {2};
  byte rule_input3_high[] = {3};
  byte rule_input4_high[] = {4};
  byte rule_input5_high[] = {5};
  byte rule_input6_high[] = {6};
  byte *g_rule_input_high[] = {rule_input1_high, rule_input2_high, rule_input3_high, rule_input4_high,
                               rule_input5_high, rule_input6_high
                              };

  // Rule Outputs
  byte rule_output1_high[] = {1};
  byte rule_output2_high[] = {1};
  byte rule_output3_high[] = {2};
  byte rule_output4_high[] = {3};
  byte rule_output5_high[] = {4};
  byte rule_output6_high[] = {5};
  byte *g_rule_output_high[] = {rule_output1_high, rule_output2_high, rule_output3_high, rule_output4_high,
                                rule_output5_high, rule_output6_high
                               };

  // Output range Min
  float output_range_min_high[] = {0};

  // Output range Max
  float output_range_max_high[] = {1};

  float fuzzy_input_high[] = {0, 0, 0, 0, 0, 0};
  float *g_fuzzy_input_high[n_inputs_high] = {fuzzy_input_high,};


  fis_evaluate(n_inputs_high, n_outputs_high, n_rules_high, g_inputs_high, g_outputs_high,
               n_mf_input_high, n_mf_output_high, g_fis_coef_input_mf_high, g_fis_coef_output_mf_high,
               g_input_mf_set_high, rule_weight_high, rule_type_high, g_rule_input_high,
               g_rule_output_high, output_range_min_high, output_range_max_high, g_fuzzy_input_high);

}
