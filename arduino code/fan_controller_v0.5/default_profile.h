// -------------------------------------- //
// Data for Fuzzy Inference System        //
// -------------------------------------- //
// Number of inputs to the FIS
const byte n_inputs_default = 2;
// Number of outputs to the FIS
const byte n_outputs_default = 1;


float g_inputs_default[n_inputs_default];
float g_outputs_default[n_outputs_default];

void init_default() {
  // Number of rules to the FIS
  const byte n_rules_default = 9;
  // Count of member function for each Input
  byte n_mf_input_default[] = {6, 3};

  // Count of member function for each Output
  byte n_mf_output_default[] = {5};

  // Coefficients for the Input Member Functions
  float coef_input_1_mf1_default[] = {0, 0, 20};
  float coef_input_1_mf2_default[] = {0, 20, 40};
  float coef_input_1_mf3_default[] = {20, 40, 60};
  float coef_input_1_mf4_default[] = {50, 65, 80};
  float coef_input_1_mf5_default[] = {60, 80, 100};
  float coef_input_1_mf6_default[] = {80, 100, 100};
  float *g_coef_input_1_mf_default[] = {coef_input_1_mf1_default, coef_input_1_mf2_default, coef_input_1_mf3_default,
                                        coef_input_1_mf4_default, coef_input_1_mf5_default, coef_input_1_mf6_default
                                       };
  float coef_input_2_mf1_default[] = {0, 0, 30};
  float coef_input_2_mf2_default[] = {20, 30, 40};
  float coef_input_2_mf3_default[] = {30, 60, 60};
  float *g_coef_input_2_mf_default[] = {coef_input_2_mf1_default, coef_input_2_mf2_default, coef_input_2_mf3_default};
  float **g_fis_coef_input_mf_default[] = {g_coef_input_1_mf_default, g_coef_input_2_mf_default};

  // Coefficients for the Output Member Functions
  float coef_output_mf1_default[] = {0, 0, 0};
  float coef_output_mf2_default[] = {0, 0, 96};
  float coef_output_mf3_default[] = {0, 0, 127};
  float coef_output_mf4_default[] = {0, 0, 159};
  float coef_output_mf5_default[] = {0, 0, 255};
  float *g_coef_output_mf_default[] = {coef_output_mf1_default, coef_output_mf2_default, coef_output_mf3_default,
                                       coef_output_mf4_default, coef_output_mf5_default
                                      };
  float **g_fis_coef_output_mf_default[] = {g_coef_output_mf_default};

  // Input membership function set
  byte input_mf_set_1_default[] = {0, 0, 0, 0, 0, 0};
  byte input_mf_set_2_default[] = {0, 0, 0};
  byte *g_input_mf_set_default[] = {input_mf_set_1_default, input_mf_set_2_default};

  // Rule Weights
  float rule_weight_default[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};

  // Rule Type
  byte rule_type_default[] = {1, 1, 1, 1, 1, 1, 1, 1, 1};

  // Rule Inputs
  byte rule_input1_default[] = {1, 0};
  byte rule_input2_default[] = {2, 0};
  byte rule_input3_default[] = {3, 0};
  byte rule_input4_default[] = {4, 0};
  byte rule_input5_default[] = {5, 0};
  byte rule_input6_default[] = {6, 0};
  byte rule_input7_default[] = {1, 3};
  byte rule_input8_default[] = {2, 3};
  byte rule_input9_default[] = {3, 3};
  byte *g_rule_input_default[] = {rule_input1_default, rule_input2_default, rule_input3_default, rule_input4_default,
                                  rule_input5_default, rule_input6_default, rule_input7_default, rule_input8_default,
                                  rule_input9_default
                                 };

  // Rule Outputs
  byte rule_output1_default[] = {1};
  byte rule_output2_default[] = {1};
  byte rule_output3_default[] = {2};
  byte rule_output4_default[] = {3};
  byte rule_output5_default[] = {4};
  byte rule_output6_default[] = {5};
  byte rule_output7_default[] = {3};
  byte rule_output8_default[] = {3};
  byte rule_output9_default[] = {3};
  byte *g_rule_output_default[] = {rule_output1_default, rule_output2_default, rule_output3_default, rule_output4_default,
                                   rule_output5_default, rule_output6_default, rule_output7_default, rule_output8_default,
                                   rule_output9_default
                                  };

  // Output range Min
  float output_range_min_default[] = {0};

  // Output range Max
  float output_range_max_default[] = {1};


  float fuzzy_input_1_default[] = {0, 0, 0, 0, 0, 0};
  float fuzzy_input_2_default[] = {0, 0, 0};
  float *g_fuzzy_input_default[n_inputs_default] = {fuzzy_input_1_default, fuzzy_input_2_default,};


  fis_evaluate(n_inputs_default, n_outputs_default, n_rules_default, g_inputs_default, g_outputs_default,
               n_mf_input_default, n_mf_output_default, g_fis_coef_input_mf_default, g_fis_coef_output_mf_default,
               g_input_mf_set_default, rule_weight_default, rule_type_default, g_rule_input_default,
               g_rule_output_default, output_range_min_default, output_range_max_default, g_fuzzy_input_default);
}
