typedef float(*_FIS_MF)(float, float *);

// -------------------------------------- //
// Functions for Fuzzy Inference System   //
// -------------------------------------- //
// Triangular Member Function
float fis_trimf(float x, float *p) {
  float a = p[0], b = p[1], c = p[2];
  float t1 = (x - a) / (b - a);
  float t2 = (c - x) / (c - b);
  if ((a == b) && (b == c)) return (float) (x == a);
  if (a == b) return (float) (t2 * (b <= x) * (x <= c));
  if (b == c) return (float) (t1 * (a <= x) * (x <= b));
  t1 = min(t1, t2);
  return (float) max(t1, 0);
}

float fis_prod(float a, float b) {
  return (a * b);
}

float fis_probor(float a, float b) {
  return (a + b - (a * b));
}

// Pointer to the implementations of member functions
_FIS_MF fis_gMF[] = { fis_trimf };

// -------------------------------------- //
// Fuzzy Inference System                 //
// -------------------------------------- //
void fis_evaluate(byte n_inputs, byte n_outputs, byte n_rules, float g_input[], float g_output[], byte n_mf_input[],
                  byte n_mf_output[], float **g_fis_coef_input_mf[], float **g_fis_coef_output_mf[],
                  byte *g_input_mf_set[], float rule_weight[], byte rule_type[], byte *g_rule_input[],
                  byte *g_rule_output[], float output_range_min[], float output_range_max[], float *fuzzyInput[]) {

  float fuzzyOutput0[] = {0, 0, 0, 0, 0};
  float *fuzzyOutput[n_outputs] = {fuzzyOutput0,};
  float fuzzyRules[n_rules] = {0};
  float fuzzyFires[n_rules] = {0};
  float *fuzzyRuleSet[] = {fuzzyRules, fuzzyFires};
  float sW = 0;

  // Transforming input to fuzzy Input
  int i, j, r, o;
  for (i = 0; i < n_inputs; ++i) {
    for (j = 0; j < n_mf_input[i]; ++j) {
      fuzzyInput[i][j] = (fis_gMF[g_input_mf_set[i][j]])(g_input[i], g_fis_coef_input_mf[i][j]);
    }
  }

  int index = 0;
  for (r = 0; r < n_rules; ++r) {
    if (rule_type[r] == 1) {
      fuzzyFires[r] = 1;
      for (i = 0; i < n_inputs; ++i) {
        index = g_rule_input[r][i];
        if (index > 0)
          fuzzyFires[r] = fis_prod(fuzzyFires[r], fuzzyInput[i][index - 1]);
        else if (index < 0)
          fuzzyFires[r] = fis_prod(fuzzyFires[r], 1 - fuzzyInput[i][-index - 1]);
        else
          fuzzyFires[r] = fis_prod(fuzzyFires[r], 1);
      }
    } else {
      fuzzyFires[r] = 0;
      for (i = 0; i < n_inputs; ++i) {
        index = g_rule_input[r][i];
        if (index > 0)
          fuzzyFires[r] = fis_probor(fuzzyFires[r], fuzzyInput[i][index - 1]);
        else if (index < 0)
          fuzzyFires[r] = fis_probor(fuzzyFires[r], 1 - fuzzyInput[i][-index - 1]);
        else
          fuzzyFires[r] = fis_probor(fuzzyFires[r], 0);
      }
    }

    fuzzyFires[r] = rule_weight[r] * fuzzyFires[r];
    sW += fuzzyFires[r];
  }

  if (sW == 0) {
    for (o = 0; o < n_outputs; ++o) {
      g_output[o] = ((output_range_max[o] + output_range_min[o]) / 2);
    }
  } else {
    for (o = 0; o < n_outputs; ++o) {
      float sWI = 0.0;
      for (j = 0; j < n_mf_output[o]; ++j) {
        fuzzyOutput[o][j] = g_fis_coef_output_mf[o][j][n_inputs];
        for (i = 0; i < n_inputs; ++i) {
          fuzzyOutput[o][j] += g_input[i] * g_fis_coef_output_mf[o][j][i];
        }
      }

      for (r = 0; r < n_rules; ++r) {
        index = g_rule_output[r][o] - 1;
        sWI += fuzzyFires[r] * fuzzyOutput[o][index];
      }

      g_output[o] = sWI / sW;
    }
  }
}
